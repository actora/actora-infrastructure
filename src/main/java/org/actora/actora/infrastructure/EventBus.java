package org.actora.actora.infrastructure;

import javax.enterprise.inject.spi.CDI;

public final class EventBus {

    private static EventBus INSTANCE;

    private EventBus(){

    }

    public static EventBus getInstance(){
        if(INSTANCE==null){
            INSTANCE = new EventBus();
        }
        return INSTANCE;
    }


    public void dispatch(ActoraEvent event){
        if(event==null)
            throw new IllegalArgumentException("event must not be null");
        CDI.current().getBeanManager().fireEvent(
                event);
    }


}
